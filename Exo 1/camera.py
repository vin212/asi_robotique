import cv2
import numpy as np
import socket
import random
import time
import time

#
UDP_IP = "127.0.0.1"
UDP_PORT = 5007

def envoyer_UDP (data):
    sock = socket.socket(socket.AF_INET, # Internet
                        socket.SOCK_DGRAM) # UDP
    d = data.flatten ()
    s = d.tostring ()
    #print(len(data[0][0]))
    #print(len(data[0]))
    #print(len(data))
    for i in xrange(20):
        sock.sendto (s[i*46224:(i+1)*46224],(UDP_IP, UDP_PORT))


if __name__=="__main__":
    
    while True :
        #cv2.destroyAllWindows()
        # Create a VideoCapture object and read from input file
        # If the input is the camera, pass 0 instead of the video file name
        cap = cv2.VideoCapture('Untitled.mp4')
        
        # Check if camera opened successfully
        if (cap.isOpened()== False): 
            print("Error opening video stream or file")
        
        # Read until video is completed
        while(cap.isOpened()):
        # Capture frame-by-frame
            ret, frame = cap.read()
            if ret == True:
        
                # Display the resulting frame
                cv2.imshow('Frame',frame)
                envoyer_UDP (frame)
        
                # Press Q on keyboard to  exit
                if cv2.waitKey(25) & 0xFF == ord('q'):
                    break
        
                # Break the loop
            else: 
                break
    
    # When everything done, release the video capture object
    #cap.release()
    
    # Closes all the frames
    #cv2.destroyAllWindows()
        
    
