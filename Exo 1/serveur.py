import socket
from threading import Thread
import numpy
import cv2



TCP_IP = '127.0.0.1'
TCP_PORT = 5006
BUFFER_SIZE = 1024

UDP_IP_VIDEO = '127.0.0.1'
UDP_PORT_VIDEO = 5007

UDP_IP = "127.0.0.1"
UDP_PORT = 5005


def serveurUDP ():
    print("UDP OK")
    sock = socket.socket(socket.AF_INET, # Internet
                        socket.SOCK_DGRAM) # UDP
    sock.bind((UDP_IP, UDP_PORT))
    while True:
        data, addr = sock.recvfrom(2280096) # buffer size is 1024 bytes
        print("received message: " + data)

def serveurUDPVideo () :
    print("UDP OK")
    s = ""
    sock = socket.socket(socket.AF_INET, # Internet
                        socket.SOCK_DGRAM) # UDP
    sock.bind((UDP_IP_VIDEO, UDP_PORT_VIDEO))

    while True:
        data, addr = sock.recvfrom(31668)
        s += data
        if len(s) == (31668*24):
            print("ici")
            frame = numpy.fromstring (s,dtype=numpy.uint8)
            frame = frame.reshape (1008,754)
            
            cv2.imshow('frame',frame)
            s = ""


def serveurTCP ():


    print("TCP OK")
    while True :
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.bind((TCP_IP, TCP_PORT))
        s.listen(1)

        conn, addr = s.accept()
        print ('Connection address:', addr)
        while True:
            data = conn.recv(BUFFER_SIZE)
            if not data: break
            print ("received data:", data)
            conn.send(data)  # echo
        conn.close()

if __name__=="__main__":
    t1 = Thread(target=serveurUDP, args=[])
    t2 = Thread(target=serveurTCP, args=[])
    t3 = Thread(target=serveurUDPVideo (), args=[])

    #t1.start()
    #t2.start()
    t3.start()

#serveurTCP()

