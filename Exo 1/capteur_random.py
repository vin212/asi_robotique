#!/usr/bin/env python3

import socket
import random
import time


TCP_IP = '127.0.0.1'
TCP_PORT = 5006
BUFFER_SIZE = 1024


def envoyer_UDP (data : str, s):
    s.send(data.encode())
    data = s.recv(BUFFER_SIZE)




if __name__=="__main__":
    
    random.seed(1)
    while True :
        valeur = random.randint(0,1024)
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((TCP_IP, TCP_PORT))
        envoyer_UDP (str(valeur), s)
        s.close()
        time.sleep(10)
    
   