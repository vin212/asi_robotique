# ASI_Robotique

* Vincent BUNIAZET
* 26 Mai 2023


***

# 1er Partie : Approches basique

## Comparaison UDP/TCP 


### Le principe :
* Le But de cette partie et de faire remonter dans un serveur des données venant de capteur diverse ainsi que d'un flux vidéo.

### __1) Le protocole UDP :__

C'est un protocole réseau qui permet d'envoyer ou de recevoir des donées. L'utilisation de celui -ci ne nécésite pas de réponse et "d'échange" entre le serveur et le cleint.

La taille de l'entête d'un UDP est de 64 bits

### __2) Le protocole TCP :__

C'est aussi un protocole réseau qui permet lui aussi d'envoyer ou de recevoir des données. L'utilisation de celui-ci nécésite un "échange" de donnée entre les deux communiquant ce qui impliquer un système obligatoire de demande réponse ou d'envoie réponse avec validation.

La taille de l'entête du TCP est de 20 octets soit 160 bites.

### __3) Comparaison deux protocoles :__

TCP est un protocole plus louds ce qui implique que le flux est plus lent qu'un UDP (avec le même débit). Cela peut être problématique pour des envoie qui doivent être rapide. Le problème de l'UDP résolut par le TCP c'est qu'on est sur que les données envoyer ont bien été recu, ce qui est très utile dans les moments critiques ou l'ont souhaite des informations de façon  sécuriser. 

![architechture récupération donné capteur](test.drawio.png)


## Execution code :
* Pour executer le code, il doit être lancer en python 3
* exemple : python3 serveur.py