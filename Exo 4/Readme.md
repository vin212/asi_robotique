### ASI_Robotique

* Vincent BUNIAZET
* 1 Juin 2023


***
# 4ème Partie

## Différent moyen de déploiement. 
---  

* On pourrais imaginer un moyen de déploiement à l'aide de docker et de gitflow zafin de faire en sorte que le programme soit déployer, cette solution permettrais aussi de mettre en place des solutions et des jeux de tests afin de vérifier la non régression.

* On pourrais imaginer aussi de le poser directement sur un cloud tel que AWS, dropbox, Méga (etc.). Cela permettrais à rendre accessible l'application mais on a des problèmes de non vérification et le fait que la mise en plus peut être plus complexe et moins efficacez qu'un déploiement en continue comme git/docker.

* On pourais imaginer aussi de fournir le service sur des machine virtuel ou sur un googlecolab. Le problème étant une dépendance très forte avec un serveur externe qui permet d'isoler le tout. Si il vient à tomber en panne, tout est à refaire sur une autre solution telle que les configurations qui peuvent être unique selon la solution.