import cv2
import numpy
import time
import sys,os
sys.path.insert(0, os.getcwd()+"/AccesData") 
from data_image import *

from flask import Flask, json, request, send_file, Response
import threading


frameData = [0,0]
nbFrame= [0]

def sendFrame(request,app):
    #print(request.stream.read())
    val = request.get_data()
    frameData[0] = frameData[1]
    if len(val) == (924480):
        frameData[1]= val
        nbFrame[0] += 1
    if nbFrame[0] >= 200:
        nbFrame[0] = 0
        saveData (frameData[0])
        

    return "Recu"

def getFrame(app):
    #print(len(frameData[0]))
    if (frameData[0] != 0):
        response = app.response_class(
            response=frameData[0],
            status=200
            #mimetype='text/html'
        )
    else :
        response = app.response_class(
            response="",
            status=201
            #mimetype='text/html'
        )
    return response

def getFrameFormat(app):
    if (frameData[0] != 0):
        return Response(gather_img(), mimetype='multipart/x-mixed-replace; boundary=frame')
    else :
        response = app.response_class(
            response="",
            status=201
            #mimetype='text/html'
        )
    return response

def getImage(imageName,app):
    if (imageName and os.path.exists("images/"+imageName)):
        return send_file("../images/"+imageName, mimetype='image/png')
    else : 
        response = app.response_class(
            response="",
            status=404
            #mimetype='text/html'
        )
        print("BUG")
    return response

def returnFrame():
    return frameData[0]

        


