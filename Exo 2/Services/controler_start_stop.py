import sys,os
sys.path.insert(0, os.getcwd()+"/Modeles")   
sys.path.insert(0, os.getcwd()+"/AccesData") 
from controler_temp import *
#print(sys.path)
from CapteursModelMarcheArret import *

marcheArret = CapteursModelMarcheArret()

def setMarcheStartStop(id,app):
    marcheArret.setCapteurEtatMarche(id)
    return setMarche(id,app)


def setArretStartStop(id,app):
    marcheArret.setCapteurEtatArret(id)
    return setArret(id,app)