from flask import Flask, json, request
import json as myJson
import sys,os
sys.path.insert(0, os.getcwd()+"/Modeles")   
sys.path.insert(0, os.getcwd()+"/AccesData") 
from CapteursTemperatureModele import *
from CapteurTemperatureModele  import *
from data_temp import *

capteurs = CapteursTemperatureModele()



def setTemp (request,app):

    print(request.json)
    if (request.json and 'id' in request.json  and 'value' in request.json and 'date' in request.json):
        capteur = CapteurTemperatureModele(str(request.json['id']),request.json['value'],request.json ['date'])
        capteurs.setCapteur(capteur)
        saveData(capteurs)

        response = app.response_class(
            status=200
        )
    else :
            response = app.response_class(
            response = "enter unvalide need : <id> and <value>",
            status=412
        )
    return response


def getTemp(request,app):
    numCapteur  = request.args.get('numCapteur')

    if (numCapteur):
        with open('data/data.json', 'r') as f:
            data = myJson.load(f)

        capteur = capteurs.getCapteur(numCapteur)
        

        if (capteur):

            data = json.dumps(capteur.toJSON())
            response = app.response_class(
                response=capteur.toJSON(),
                status=200,
                mimetype='application/json'
            )
        else :
            response = app.response_class(
            response=json.dumps(""),
            status=201,
            mimetype='application/json'
        )
    else :
        response = app.response_class(
            response = "numCapteur is missing",
            status=412
        )
    return response


def setMarche(id,app):
    capteurs.setCapteurMarche(id)
    saveData(capteurs)
    response = app.response_class(
                status=200,
                mimetype='application/json'
            )
    return response


def setArret(id,app):
    capteurs.setCapteurArret(id)
    saveData(capteurs)
    response = app.response_class(
                status=200,
                mimetype='application/json'
            )
    return response
