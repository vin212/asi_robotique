### ASI_Robotique

* Vincent BUNIAZET
* 30 Mai 2023


***
# 2nd Partie 

## Avantage HTTP/HTTPS : 
---  
* L'avantage de ce protocole c'est qu'il est normaliser, il permet d'envoyer des informations en ligne sans trop de difficultés. Aujourd'hui, il existe de plus en plus d'outils pour le mettre en place ce qui facilite d'autant plus sont utilisation. 

* Grace que HTTP/HTTPS, nous pouvons assi réaliser des interface web et avoir une connection entre nos capteur et notre interface grpahique. L'avangage de celui-ci c'est qu'il nous permet d'avoir une interface visuel sur différentes platforme tel qu'un téléphone, tablette ou ordinateur sans avoir l'obligation de développer pour chacune des technologies ou de devoir addapter en fonction de l'appreil. De plus, le développement web est de plus en plus responsif ce qui permet de faire en sortie que ce soit lisible facilement sur tout les appareil.

* L'avantage du HTTP/HTTPS c'est qu'il peut aussi permet de communique entre différentes technologie (différent langage, différent programme etc.) Sans avoir à avoir un fichier commun de typage.

* HTTPS propose aussi une solution d'encryptage ce qui permet de protéger les données qui se "déplace" sur le réseau.



## Architecture :
---

![architecture service robotique](archi_service.drawio.png)

## Développement exemple :

### __Update la valeur d'un capteurs__
Requête : 

```
curl -X POST localhost:5000/setCapteur  -H 'Content-Type: application/json' -d '{"id":1,"value":8}'
```

### __Get la valeur d'un capteurs (id 1)__

Requête :
```
curl -X GET localhost:5000/getCapteur?numCapteur=2
```

Réponse :
```
{"1": "8"}
```

## Ce qui à été réaliser :
---
* Le serveur ce trouve dans "Controleurs/serveur_flask.py" 
```
export FLASK_APP=Controleurs/serveur_flask.py
python -m flask run --with-threads
```

* Pour ajouter/lancer un capteur dans "Clients/UnCapteur.py"
```
python Clients/UnCapteur.py 1
```
La commande permet de crée un capteur d'id 1 qui génère des donnée entre -10 et 25 et les envoie au serveur toute les 10 secondes.

* Pour avoir un retour "Clients/AfficherUnCapteur
```
python Clients/AfficherUnCapteur.py 1 2
```
Il va intéroger le serveur pour connaitre la valeur et l'état du capteur d'id 1 et 2 (il peut afficher les état de plusieurs capteur selectionner).


* Pour lancer la simulation d'une caméra.
* __NB__: Le fichier vidéo dans le dossier source nommé "Untitled.mp4" est nécessaire pour la simulation.
```
python Clients/SimulationCamera.py 
```
Il permet de lire un flux vidéo et de le publier sur sur un serveur. 

* Pour afficher le flux de la caméra disponible sur le serveur.
```
python Clients/AfficherCamera.py 
```
Il permet de demander les images d'une vidéo disponible sur le serveur.

* Pour afficher une image enregister :
```
    http://127.0.0.1:5000/getImage/test2023-05-31%2009:44:18.964899.png
```

Soit : 
```
    http://127.0.0.1:5000/getImage/<imageName>
```

* Exemple d'une execution, un premier terminal est lancer (le serveur), un second est lancet (la simulation de la caméra) et un troisème (le client qui traite le flux et réalise un traitement d'image sur une frame du flux vidéo)
![exemple execution traitement image edge](traitementImage.gif)


* Exemple d'envoie et de récéption des données, dans le 1er terminal, on allume le serveur, dans le terminal du haut, on lance un capteur d'id 1, dans le terminal du bas, on lance un capteur d'id 8, et dans le dernier teminal, on affiche les valeurs des deux capteurs.
![exemple envoie et reception data capteur](data.gif)

## Ce qui a été développer :
---
* Un serveur qui permet de collecter les donner. Le serveur étant conforme à l'architecture, il porpose un contorleur que sert de "porte d'entrée" de l'API. Dans ce même serveur, on retrouve différent Servicec (disponible dans le dossier Service) qui vérifie la validiter des données et map les données si besoin pour qu'ils soient compatible avec un modèle définie. Pour le modèles chaque objet névcésitant une modélisation avec un modèle est porposer dans Modèle. Son seul but est de représenter et de faire en sorte de stocker des versions réel en virtual. 

* Nous avons différents client qui soit publie des données (envoie des donnée à l'API avec le PUT ou POST), soit récupère des données afin de les affichers ou de les traiter.

* Pour finir, différents fonctionaliter son présente
    1) La valeurs des capteurs (qui sont simuler avec une génération aléatoir)
    2) L'état des capteurs qui définisse si le capteur est allumer ou éteint
    3) Un flux vidéo pouvant être écrit et lu

        3.a) Pour ce qui est du flux vidéo, plusieurs solution on été mis en place, la publication de la vidéo par des PUT frame par frame
        
        3.b) La lecture de la vidéo frame par frame par des GET
        
        3.c) La lecture de la vidéo par le bias d'un flux => celui-ci est utilise afin de réaliser des traitements d'image (ici détéction des bordures)

## Les données :
---
    * Les données des capteurs sont stocker dans "data", ajourd'hui, le fichier est écraser quand le serveur redémar mais il pourrais éventuellement être relu pour être réutiliser.
    * Les Images sont stocker dans "Images" on stocke une image toute les 1000 images et on stocke l'image cpourante (une capture de la vidéo)
    * Une vidéo utile pour la simulation du streaming

## Problème éventuel : 
---

* Le problème d'une telle architecture en flask, c'est qu'il atteint rapidement ses limites, a plus d'une personne qui essaye d'acceder 

## Documentation API
--- 
* Vérifier si le serveur fonction 
```
/
```
=> Affiche "Hello world"

* Récupérer la valeur d'un capteur donné 
```
/getCapteur?numCapteur=<id>
```
=> retourn un Json avec les valeurs des capteur  
```
curl -X GET localhost:5000/getCapteur?numCapteur=2

retour : 
{
    "date": "2023-06-01 14:42:53.190001", 
    "idCapteur": "1", 
    "marche": true, 
    "valeur": 11
}
```

* Changer les valeur et ajouter un capteur si besoin
```
/setCapteur
```
=> pas de retour
```
curl -X POST localhost:5000/setCapteur  -H 'Content-Type: application/json' -d '{"id":1,"value":8,"date":2023-06-01 14:46:53.190001}' 
```

* Informer que la capteur est allumer
```
/setCapteurMarche/<int:id>
```
=> pas de retour

```
curl -X POST localhost:5000/setCapteurMarche/1 
```

* Informer que la capteur est Eteint
```
/setCapteurArret/<int:id>
```
=> pas de retour

```
curl -X POST localhost:5000/setCapteurArret/1 
```

---
* Envoyer une frame (pour du frame par frame)
```
/sendFrame
```
=> pas de retour

* Permet de récupérer une frame (pour du frame par frame)
```
/getFrame
```
=> Retourne la dernier image envoyer par la caméra (ou envoyer tout court)

* Permet de récupérer une images sauvegarder avec le nom de l'image
```
/getImage/<string:imageName>
```
=> retourne l'image sauvegarder demander

* Permet de récupérer un flux vidéo
```
/getFrameFormat
```
=> retourne un flux vidéo de type mjpeg

---

## Partie 2 (bis)

* Nvidia pytriton est un framwork qui permet de faire une interface avec des IA et ainsi d'optimiser. Elle permet aussi (par la normalisation des entrée sortie) de pouvoire utiliser diffrérent type de langage et de framwork qui vont communiquer avec celui-ci. Et il est optimiser pour le DevOps ce qui permet de réduire le temps de développement et de déploierment.

---

## Partie 2 (tiers)

* D'autre outils dans d'autre langage peuvent être utiliser telle que springboot en java qui permet de réaliser la même chose, le développement d'un serveur sous PHP (presque natif) avec Apache. 

