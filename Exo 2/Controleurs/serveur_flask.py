from flask import Flask, json, request, Response

import numpy
import time
import sys
import os
import cv2
import threading
sys.path.insert(0, os.getcwd()+"/Services")    
#print(sys.path)
from controler_temp import *
from controler_start_stop import *
from controler_camera import *

app = Flask(__name__)
app.threaded = True

IMG_FOLDER = os.path.join('images')

app.config['UPLOAD_FOLDER'] = IMG_FOLDER

@app.route("/")
def hello_world():
    return "<p>Hello, World!</p>"

@app.route("/getCapteur", methods=['GET'])
def getTempEnter():
    return getTemp(request,app)

@app.route("/setCapteur", methods=['POST'])
def setTempEnter():
    return setTemp(request,app)

@app.route("/setCapteurMarche/<int:id>", methods=['POST'])
def setTempMarcheEnter(id):
    return setMarcheStartStop(str(id),app)

@app.route("/setCapteurArret/<int:id>", methods=['POST'])
def setTempArretEnter(id):
    return setArretStartStop(str(id),app)

@app.route("/sendFrame", methods=['POST'])
def sendFrameEnter():
    return sendFrame(request,app)

@app.route("/getFrame", methods=['GET'])
def getFrameEnter():
    return getFrame(app)

@app.route("/getImage/<string:imageName>",methods=['GET'])
def getImageEnter(imageName):
    return getImage(imageName,app)


@app.route("/getFrameFormat",methods=['GET'])
def getFrameFormatEnter():
    def gather_img():
        while True :
            frameData = returnFrame()
            #time.sleep(0.1)
            frame = numpy.fromstring (frameData,dtype=numpy.uint8)
            frame = frame.reshape (480,642,3)
            success, encoded_frame= cv2.imencode('.jpg', frame)
            yield (b'--frame\r\nContent-Type: image/jpeg\r\n\r\n' + encoded_frame.tobytes() + b'\r\n')
            
        #return getFrameFormat()
    return Response(gather_img(), mimetype='multipart/x-mixed-replace; boundary=frame')

#app.run(host='127.0.0.1', threaded=True)

if __name__ == "__main__":
    app.run(host='127.0.0.1', port=5000, threaded=True)