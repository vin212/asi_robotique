
import cv2
import urllib
import numpy as np
import time
import urllib
import time
import matplotlib.pyplot as plt



def traiterImage(frame):
    b,g,r = cv2.split(frame)
    frame_rgb = cv2.merge((r,g,b))
    new_frame = np.copy(frame_rgb)

    for e in range(1,len(frame_rgb)-1):
        for i in range(1,len(frame_rgb[e])-1):
            val = abs(np.mean(frame_rgb[e][i-1]) - np.mean(frame_rgb[e][i+1]))
            val2 = abs(np.mean(frame_rgb[e-1][i]) - np.mean(frame_rgb[e+1][i]))

            val = val + val2
            if (val > 20):
                new_frame[e][i][0] = 0
                new_frame[e][i][1] = 0
                new_frame[e][i][2] = 0
            else:
                new_frame[e][i][0] = 255
                new_frame[e][i][1] = 255
                new_frame[e][i][2] = 255
                 

    r, g, b = cv2.split(new_frame)
    frame_bgr = cv2.merge((b,g,r))

    cv2.imshow('image',frame_bgr )
    
    cv2.waitKey(2000)
    cv2.destroyWindow('image')
    


if __name__ == "__main__":
    stream=urllib.urlopen('http://127.0.0.1:5000/getFrameFormat')
    bytes=''
    nbFrame = 0
    while True:
        bytes+=stream.read(1024)
        a = bytes.find('\xff\xd8') # JPEG start
        b = bytes.find('\xff\xd9') # JPEG end
        if a!=-1 and b!=-1:
            jpg = bytes[a:b+2] # actual image
            bytes= bytes[b+2:] # other informations

            # decode to colored image ( another option is cv2.IMREAD_GRAYSCALE )
            img = cv2.imdecode(np.fromstring(jpg, dtype=np.uint8),cv2.IMREAD_COLOR) 
            nbFrame +=1 
            #print(nbFrame)
            if (nbFrame == 1000):
                traiterImage(img)
                nbFrame = 0

            cv2.imshow('Window name',img) # display image while receiving data
            if cv2.waitKey(1) ==27: # if user hit esc
                exit(0) # exit program