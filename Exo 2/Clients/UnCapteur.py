import requests
import random
import time, datetime
import sys, signal

url = 'http://127.0.0.1:5000/setCapteur'
urlMarche = 'http://127.0.0.1:5000/setCapteurMarche'
urlArret = 'http://127.0.0.1:5000/setCapteurArret'



def signal_handler(sig, frame):
    print('You pressed Ctrl+C!')
    requests.post(urlArret  + "/" + str(sys.argv[1]))
    sys.exit(0)

if __name__ == "__main__":

    signal.signal(signal.SIGINT, signal_handler)
    if (len(sys.argv) == 2):

        requests.post(urlMarche  + "/" + str(sys.argv[1]))
        while True :
            valeur = random.randint(-10,25)
            current_time = datetime.datetime.now()
            myobj = {'id': sys.argv[1],'value':valeur, 'date' : str(current_time)}
            print("ajout de : " + str(valeur) + " id :  "+ str(sys.argv[1]))

            x = requests.post(url, json = myobj)

            print(x.text)

            time.sleep(10)
    else :
        print("Il manque un ID")
