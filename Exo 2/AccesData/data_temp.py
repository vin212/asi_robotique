import sys,os
sys.path.insert(0, os.getcwd()+"/Modeles")   
from CapteursTemperatureModele import *

fileName = "data/data.json"

def saveData (capteurs):
    file = open(fileName,"w")
    file.write(capteurs.toJSON())
    file.close()