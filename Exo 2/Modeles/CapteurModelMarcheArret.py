class CapteurModelMarcheArret:

    def __init__(self, id, etat):
        self.id = id
        self.etat = etat

    def setCapteurMarche(self):
        self.etat = True

    def setCapteurArret(self):
        self.etat = False

    def getCapteurEtat(self):
        return self.etat
    
    def getId(self):
        return self.id

    def __str__(self):
        return "le capteur : " + str(self.id) + " est : " + str(self.etat)
        