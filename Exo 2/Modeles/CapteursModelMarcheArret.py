from flask import Flask, json, request
import json as myJson
import sys,os
sys.path.insert(0, os.getcwd()+"/Modeles")   
sys.path.insert(0, os.getcwd()+"/AccesData") 
from CapteurModelMarcheArret import *


class CapteursModelMarcheArret:

    def __init__(self):
        self.capteur = []
    
    def setCapteurEtatMarche(self,id):
        i = 0
        while len(self.capteur) > i and self.capteur[i].getId() != id:
            i = i + 1
        
        if i < len(self.capteur):
            self.capteur[i].setCapteurMarche()
        else :
            unCapteur = CapteurModelMarcheArret(id,True)
            self.capteur.append(unCapteur)
            print("APPEND")

    
    def setCapteurEtatArret(self,id):
        i = 0
        while len(self.capteur) > i and self.capteur[i].getId() != id:
            i = i + 1
        
        if i < len(self.capteur):
            self.capteur[i].setCapteurArret()
        else :
            unCapteur = CapteurModelMarcheArret(id,False)
            self.capteur.append(unCapteur)

    def __str__(self):
        retour = ""
        for e in self.capteur:
            retour += str(e) + "\n"
        return retour