import json

from CapteurTemperatureModele import *

class CapteursTemperatureModele :

    def __init__(self):
        self.capteurs = []

    def setCapteur(self, capteur):
        i = 0
        while i < len(self.capteurs) and self.capteurs[i].getIdCapteur() != capteur.getIdCapteur ():
            i = i + 1
        
        if (i < len(self.capteurs)):
                self.capteurs[i].setValeur(capteur.valeur)
                self.capteurs[i].setDate( capteur.date)
        else :
            self.capteurs.append(capteur)

    
    def setCapteurMarche(self, id):
        i = 0
        while i < len(self.capteurs) and self.capteurs[i].getIdCapteur() != id:
            i = i+ 1
        if (i < len(self.capteurs)):
            self.capteurs[i].setMarche(True)
        else :
            capteur = CapteurTemperatureModele(id,None,None)
            capteur.setMarche(True)
            self.capteurs.append(capteur)




    def setCapteurArret(self, id):
        i = 0
        while i < len(self.capteurs) and self.capteurs[i].getIdCapteur() != id:
            i = i+ 1
        if (i < len(self.capteurs)):
            self.capteurs[i].setMarche(False)
        else :
            capteur = CapteurTemperatureModele(id,None,None)
            capteur.setMarche(False)
            self.capteurs.append(capteur)


        
    
    def getCapteur (self,id):
        i = 0
        while i < len(self.capteurs) and self.capteurs[i].getIdCapteur() != id:
            i = i+ 1
        
        if (i < len(self.capteurs)):
            capteur = self.capteurs[i]
        else :
            capteur = None

        return capteur
        
    def __str__(self):
        valeur = ""
        for e in self.capteurs :
            valeur += str(e)
            valeur += '\n'
        return valeur
    
    def toJSON(self):
        return json.dumps(self, default=lambda o: o.__dict__, 
            sort_keys=True, indent=4)