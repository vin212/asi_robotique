import json

class CapteurTemperatureModele :

    def __init__(self,idCapteur,valeur,date = None):
        self.idCapteur = idCapteur
        self.valeur = valeur
        self.date = date
        self.marche = False
    
    def getIdCapteur (self):
        return self.idCapteur
    
    def getDate (self):
        return self.date
    
    def getValeur (self):
        return self.valeur
    
    def setValeur (self, valeur):
        self.valeur = valeur

    def setDate (self, date):
        self.date = date
    
    def setMarche(self, valeur):
        self.marche = valeur

    def __str__(self):
        return "Capteur d id : " + str(self.idCapteur) + " a comme valeurs : " + str(self.valeur) + " a : " + str(self.date)
    
    def toJSON(self):
        return json.dumps(self, default=lambda o: o.__dict__, 
            sort_keys=True, indent=4)